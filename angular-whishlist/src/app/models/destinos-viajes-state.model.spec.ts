import {
  reducerDestinosViajes,
  DestinosViajesState,
  intializeDestinosViajesState,
  InitMyDataAction,
  NuevoDestinoAction,
  ElegidoFavoritoAction,
  VoteUpAction,
  VoteDownAction,
  CuentaTrackingAction
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
  it('should reduce init data', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
  });

  it('should reduce new item added', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('barcelona');
  });

  it('should reduce elegido favorito', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(new DestinoViaje('favorito', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.favorito.nombre).toEqual('favorito');
    expect(newState.favorito.selected).toEqual(true);
  });

  it('should reduce votar bien', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: VoteUpAction = new VoteUpAction(new DestinoViaje('votarUP', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(action.destino.nombre).toEqual('votarUP');
    expect(action.destino.votes).toEqual(1);
  });

  it('should reduce votar mal', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: VoteDownAction = new VoteDownAction(new DestinoViaje('votarDOWN', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(action.destino.nombre).toEqual('votarDOWN');
    expect(action.destino.votes).toEqual(-1);
  });

  it('should reduce tracking', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: CuentaTrackingAction = new CuentaTrackingAction(['destino_vote_up', 'destino_vote_down', 
                                                                  'destino_elegir_preferido', 'lista_destinos_item']);
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.votosUp).toEqual(1);
    expect(newState.votosDown).toEqual(1);
    expect(newState.clicks).toEqual(1);
    expect(newState.fav).toEqual(1);
  });
});