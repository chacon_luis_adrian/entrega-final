import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { AppState } from './app.module';
import { Store } from '@ngrx/store';
import { CuentaTrackingAction} from './models/destinos-viajes-state.model';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;

  constructor(private elRef: ElementRef, private store: Store<AppState>) {
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento));
  }

  track(evento: Event): void {
    const elemTags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
    console.log(`||||||||||| track evento: "${elemTags}"`);
    this.store.dispatch(new CuentaTrackingAction(elemTags));

  }
}
