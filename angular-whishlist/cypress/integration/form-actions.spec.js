describe('Formulario de agregar', () => {
    it('Agregando un destino', () => {
        cy.visit('http://localhost:4200');
        cy.get('#nombre').type('Primer Destino').should('have.value', 'Primer Destino');
    });

    it('Haciendo click y validando', () => {
        cy.get('.btn-primary').click();
        cy.get('h3').should('contain', 'Primer Destino');
    });
});